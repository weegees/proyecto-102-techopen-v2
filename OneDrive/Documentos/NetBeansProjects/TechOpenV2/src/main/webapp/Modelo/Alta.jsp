<%-- 
    Document   : Alta
    Created on : 7/06/2020, 05:56:47 AM
    Author     : jorje
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import= "java.sql.*" %>
<!DOCTYPE html>
<html>
    <head>
        <title>TechOpen: Técnicos a tu alcance</title>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
        <link href='https://fonts.googleapis.com/css?family=Roboto' rel='stylesheet'>
        <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
        <link rel="stylesheet" href=".../visual/estilos.css">
        <script type="text/javascript" src=".../js/jquery-1.7.2.min.js"></script>
        <style>
            h1 {text-align: center;}
            p {text-align: center;}
            div {text-align: center;}
        </style>

    </head>

    <body>
        <br><br/>
        <h1><center><img src=".../visual/entra.png"></center></h1>
        <br><br/>


        <%
            String nombre = request.getParameter("Nombre");
            String area = request.getParameter("area");
            String opinion = request.getParameter("opinion");
            String calificacion = request.getParameter("calificacion");

            Connection con = null;
            Statement st = null;

            try {

                Class.forName("com.mysql.cj.jdbc.Driver");
                con = (Connection) DriverManager.getConnection("jdbc:mysql://127.0.0.1/techopen?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC&useSSL=false", "root", "root");
                st = con.createStatement();

                if (!nombre.isEmpty() && !area.isEmpty() && !calificacion.isEmpty()) {

                    String consulta = "INSERT INTO `techopen`.`registros` (`Nombre`, `Calificaciones_id`, `Opinion`, `Area`) VALUES ('" + nombre + "', '" + calificacion + "', '" + opinion + "', '" + area + "');";
                    st.executeUpdate(consulta);
        %>

    <center>
        <p><strong>¡El técnico ha sido dado de alta exitosamente! </strong></p> 

        <%
        } else {

        %>

        <p><strong>Oops! Parece que los datos introducidos han sido incorrectos. ¡Intentalo de nuevo! </strong></p>

        <%                }

        %>

        <%                } catch (Exception e) {

        %>

        <p><strong>Oops! Parece que los datos introducidos han sido incorrectos. ¡Intentalo de nuevo! </strong></p>


        <%                

            }
        %>

        <br><br/>


        <a href=".../index.jsp">
            <img src=".../visual/inicio.png" style="max-width:04%;width:auto;height:auto;" class="center">
        </a>


    </center>
</body>
</html>
