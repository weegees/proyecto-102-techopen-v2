<%-- 
    Document   : index
    Created on : 7/06/2020, 04:25:49 AM
    Author     : Jorge Gonz�lez
--%>

<%@ page contentType="text/html; charset=ISO-8859-1" 
         import ="java.sql.*"        

         %>
<!DOCTYPE html>
<html>
    <head>
        <title>TechOpen: T�cnicos a tu alcance</title>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
        <link href='https://fonts.googleapis.com/css?family=Roboto' rel='stylesheet'>
        <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
        <link rel="stylesheet" href="visual/estilos.css">
        <script type="text/javascript" src="js/cambiarPestanna.js"></script>
        <script type="text/javascript" src="js/jquery-1.7.2.min.js"></script>


    </head>

    <body>
        <br><br/>
        <h1><center><img src="visual/entra.png"></center></h1>
        <br><br/>

    <body onload="javascript:cambiarPestanna(pestanas, pestana1);" />

    <div class="contenedor">
        <div id="pestanas">
            <ul id=lista>
                <li id="pestana1"><a href='javascript:cambiarPestanna(pestanas,pestana1);'>Inicio</a></li>
                <li id="pestana2"><a href='javascript:cambiarPestanna(pestanas,pestana2);'>Dar de alta</a></li>
                <li id="pestana3"><a href='javascript:cambiarPestanna(pestanas,pestana3);'>Modificaciones</a></li>
                <li id="pestana4"><a href='javascript:cambiarPestanna(pestanas,pestana4);'>Bajas</a></li>
                <li id="pestana5"><a href='javascript:cambiarPestanna(pestanas,pestana5);'>T�cnicos en alta</a></li>

            </ul>
        </div>

        <div id="contenidopestanas">

            <div id="cpestana1">

                <p>�TechOpen te permite conectarte con los t�cnicos al alcance de tu zona! Puedes dar de alta nuevos t�cnicos en tu zona, darlos de baja
                    de la plataforma, calificarlos o modificar sus datos. 

                    <br>
                    <br/>

                    Esta iniciativa se encuentra en una fase muy temprana de desarrollo. �Adelante!

                </p>

            </div>

            <div id="cpestana2">

                �Bienvenido! En este apartado puedes agregar un t�cnico que conozcas a la base de t�cnicos disponibles en tu zona. Para ello, se te solicitar�n
                diversos datos con el fin de poder agregarlo exitosamente a la lista de disponibilidad. 

                <form name="Registro" action="Modelo/Alta.jsp" method="get">

                    <center>
                        <p><strong>Ingresa el nombre del t�cnico: </strong></p>

                        <textarea name="Nombre" rows="2" cols="45" wrap="hard" placeholder="Escribe aqu� el nombre del t�cnico."></textarea>

                        <p></p>

                        <label for="area">�A qu� �rea se dedica?:</label>
                        <select name="area" id="area">
                            <option value="Alba�iler�a">Alba�iler�a</option>
                            <option value="Alba�iler�a">Automotr�z</option>
                            <option value="Computaci�n">Computaci�n</option>
                            <option value="Electricista">Electricista</option>
                            <option value="Electr�nica">Electr�nica</option>
                            <option value="Plomer�a">Plomer�a</option>
                            <option value="Telecomunicaciones">Telecomunicaciones</option>
                        </select>

                        <br>
                        <br/>

                        <p><strong>Introduce una opini�n del t�cnico: </strong></p>

                        <textarea name="opinion" rows="10" cols="100" wrap="hard" placeholder="Escribe aqu� tu opini�n."></textarea>

                        <p></p>


                        <p><strong>Selecciona una calificaci�n:</strong></p>

                        <img src="visual/star1.jpg" style="max-width:03%;width:auto;height:auto;"><input type="radio" id="1" name="calificacion" value="1">
                        <label for="male">Deficiente</label><br>
                        <br/>

                        <img src="visual/star2.jpg" style="max-width:02.6%;width:auto;height:auto;"><input type="radio" id="2" name="calificacion" value="2">
                        <label for="female">Poco indicado</label><br>
                        <br/>

                        <img src="visual/star3.jpg" style="max-width:02.6%;width:auto;height:auto;"><input type="radio" id="3" name="calificacion" value="3">
                        <label for="other">Regular</label><br>
                        <br/>

                        <img src="visual/star4.jpg" style="max-width:03%;width:auto;height:auto;"><input type="radio" id="4" name="calificacion" value="4">
                        <label for="male">Bueno</label><br>
                        <br/>

                        <img src="visual/star5.jpg" style="max-width:03%;width:auto;height:auto;"><input type="radio" id="5" name="calificacion" value="5">
                        <label for="male">Excelente</label><br>
                        <br/>


                    </center>
                    <center>
                        <p></p>
                        <input type="submit" value="Dar de alta el t�cnico en tu zona">
                    </center>
                </form>


                <p></p>
                <center>
                    </br>


            </div>
            <div id="cpestana3">
                
                En esta secci�n puedes modificar los datos de un t�cnico que est� registrado en la base de t�cnicos disponibles en tu zona. Se te solicitar� que
                ingreses el identificador (ID) del t�cnico en la base con el fin de poder realizar la modificaci�n exitosamente. 

                <form name="Registro" action="Modelo/Modificaciones.jsp" method="get">

                    <center>
                        

                        <p><strong>Ingresa el ID del t�cnico: </strong></p> <input type="text" name="ID" value=""/> <p><br>

                        <p></p>


                        <p><strong>Introduce una nueva opini�n del t�cnico: </strong></p>

                        <textarea name="opinion" rows="10" cols="100" wrap="hard" placeholder="Escribe aqu� tu opini�n."></textarea>

                        <p></p>


                        <p><strong>Selecciona una nueva calificaci�n:</strong></p>

                        <img src="visual/star1.jpg" style="max-width:03%;width:auto;height:auto;"><input type="radio" id="1" name="calificacion" value="1">
                        <label for="male">Deficiente</label><br>
                        <br/>

                        <img src="visual/star2.jpg" style="max-width:02.6%;width:auto;height:auto;"><input type="radio" id="2" name="calificacion" value="2">
                        <label for="female">Poco indicado</label><br>
                        <br/>

                        <img src="visual/star3.jpg" style="max-width:02.6%;width:auto;height:auto;"><input type="radio" id="3" name="calificacion" value="3">
                        <label for="other">Regular</label><br>
                        <br/>

                        <img src="visual/star4.jpg" style="max-width:03%;width:auto;height:auto;"><input type="radio" id="4" name="calificacion" value="4">
                        <label for="male">Bueno</label><br>
                        <br/>

                        <img src="visual/star5.jpg" style="max-width:03%;width:auto;height:auto;"><input type="radio" id="5" name="calificacion" value="5">
                        <label for="male">Excelente</label><br>
                        <br/>


                    </center>
                    <center>
                        <p></p>
                        <input type="submit" value="Modificar!">
                    </center>
                </form>


                <p></p>
                <center>
                    </br>
                
                
                
                
                
                
            </div>

            <div id="cpestana4">
                Si deseas dar de baja el contacto de uno de los t�cnicos alrededor de tu zona, introduce su respectivo ID con el fin de eliminarlo de la lista.
                
                <center>
        <form name="Registro" action="Modelo/Bajas.jsp" method="get">
            
            <p><strong>Ingresa el ID del t�cnico: </strong></p> <input type="text" name="ID" value=""/> <p><br>
                
            <input type="submit" value="Eliminar">
            
            <p></p>
            
        </form>
        
        <p></p>
                
                 
            </div>

            <div id="cpestana5">
                <table border bgcolor="#F0F8FF" style="margin: 0 auto; color: black">
                    <p><strong><marquee behavior=alternate><font color="white"><center>En la siguiente tabla encontrar�s los t�cnicos dados de alta en tu zona.</center></font></marquee></strong></p>

                    <center>
                        <tr>
                            <th><font color="Peru">ID de la entrada</font></th>
                            <th><font color="Peru">Nombre del t�cnico</font></th>
                            <th><font color="Peru">�rea</font></th>
                            <th><font color="Peru">Fecha de alta</font></th>
                            <th><font color="Peru">Calificaci�n</font></th>
                            <th><font color="Peru">Opini�n</font></th>
                        </tr>


                        <%
                            Connection conex = null;
                            Statement sql = null;
                            try {
                                Class.forName("com.mysql.cj.jdbc.Driver");
                                conex = (Connection) DriverManager.getConnection("jdbc:mysql://127.0.0.1/techopen?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC&useSSL=false", "root", "root");
                                sql = conex.createStatement();
                                String qry = "select * from techopen.registros";
                                ResultSet data = sql.executeQuery(qry);
                                while (data.next()) {
                        %>

                        <% if (data.getInt("Visibilidad") == 1) { %>
                        <tr>
                            <td>
                                <% out.print(data.getInt("id"));%>
                            </td>
                            <td>
                                <% out.print(data.getString("Nombre"));%>
                            </td>
                            <td>
                                <% out.print(data.getString("Area"));%>
                            </td>
                            <td>
                                <% out.print(data.getString("Fecha de alta"));%>
                            </td>
                            <td>
                                <%
                                    if (Integer.parseInt(data.getString("Calificaciones_id")) == 1) {

                                        out.print("Deficiente");

                                    } else if (Integer.parseInt(data.getString("Calificaciones_id")) == 2) {

                                        out.print("Poco indicado");

                                    } else if (Integer.parseInt(data.getString("Calificaciones_id")) == 3) {
                                        out.print("Regular");

                                    } else if (Integer.parseInt(data.getString("Calificaciones_id")) == 4) {
                                        out.print("Bueno");

                                    } else if (Integer.parseInt(data.getString("Calificaciones_id")) == 5) {
                                        out.print("Excelente");

                                    }


                                %>
                            </td>
                            <td>
                                <% out.print(data.getString("Opinion"));%>
                            </td>
                        </tr>

                        <%  } %>

                        <%
                                }
                                data.close();

                            } catch (Exception e) {
                                out.print("Error en la conexi�n con los registros. :( Intenta de nuevo!");
                                
                            }

                        %>
                </table>
                </br>



            </div>

        </div>
    </div>


    <br/><br/><br/><br/><br/>

    <br><br/>
    <footer><center><img src="visual/imagen0.png"></center></footer>.
    <br><br/>
</body>



</html>